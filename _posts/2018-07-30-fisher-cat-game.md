---
layout:     post
title:      Cat Fisher Game
date:       2018-07-30 11:55:19
summary:    See what the different elements looks like.
categories: jekyll
thumbnail: cogs
tags:
 - game
 - ue4
 - unreal engine
 - woohoo
---

# Picture of the bad boi

Our lovely `artist` made this `bad boi`! <3

![Thumper](https://pbs.twimg.com/media/Di-iRqWXcAAN4Q-.png)

### Movement Code

{% highlight c++ %}
void ACat::Movement(float DeltaTime)
{
	if (movementInput.SizeSquared() > FMath::Square(deadZone))
	{
		movementInput.Normalize();
		movementComponent->Velocity = (FVector(movementInput.X, movementInput.Y, 0.0f) * MovementSpeed)
		+ movementComponent->Velocity.Z;
		
		FVector MovementVector = FVector(movementInput.X, movementInput.Y, 0.0f);
		FRotator NewRotation = MovementVector.ToOrientationRotator();
		FRotator MovementRot = FMath::RInterpTo(GetActorRotation(), NewRotation, DeltaTime, RotationInterpSpeed);

		// Rotate our actor
		GetController()->SetControlRotation(MovementRot);
	}
}
{% endhighlight %}

We still gotta do:

* Camera
* Polish player movement


### See you soon!

> Never - Dothi 2018